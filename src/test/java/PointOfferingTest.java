import org.testng.annotations.AfterMethod;
import org.testng.annotations.Test;
import utils.UsersDataProvider;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalToIgnoringCase;

public class PointOfferingTest extends BaseAbstract {


    @Test
    public void givePointstoself() {
        loginPage.setUsername("arus").setPassword("pass").clickSubmit();
        offerPointsPage.typeUser("arus").expandCriteria().setRandomCriteria();
        offerPointsPage.expandPoints().setRandomPoints();
        offerPointsPage.writeRandomComment().clickSubmit();
        String warning = offerPointsPage.getWarning();
        assertThat("", warning, equalToIgnoringCase("You cannot give Multam Fain points to yourself! You little rascal..."));
    }

    @Test
    public void givePoints() {
        loginPage.setUsername("arus").setPassword("pass").clickSubmit();
        offerPointsPage.typeUser("bogdan.tiu").expandCriteria().setRandomCriteria();
        offerPointsPage.expandPoints().setRandomPoints();
        offerPointsPage.writeRandomComment().clickSubmit();
        String confirmation = offerPointsPage.getConfirmationMessage();
        assertThat("nu apare mesajul", confirmation, equalToIgnoringCase("Saved!"));
    }

    @AfterMethod
    public void logOut() {
        headerPage.clickLogout();
    }


}
