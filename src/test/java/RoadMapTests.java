import org.testng.annotations.AfterMethod;
import org.testng.annotations.Test;
import utils.ExperienceLevel;

import java.util.ArrayList;
import java.util.List;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.*;


public class RoadMapTests extends BaseAbstract {

    @Test
    public void setNewRoadmap() {
        String mentor = "arus";
        String mentee = "vrozsa";

        String dayDiscussion = "30";
        String monthDiscussion = "05";
        String yearDiscussion = "2019";

        String dayobjectioves = "31";
        String monthObjectives = "05";
        String yearObjectives = "2019";

        String automationLevel = ExperienceLevel.JUNIOR_ENTRY.getLevel();
        String manualLevel = ExperienceLevel.JUNIOR_ENTRY.getLevel();

        String notes = "niciuna";
        String objectives = "fara";

        ArrayList<String> parameterList = new ArrayList<>();
        parameterList.add(yearDiscussion + "-" + monthDiscussion + "-" + dayDiscussion);
        parameterList.add(mentor);
        parameterList.add(automationLevel);
        parameterList.add(manualLevel);
        parameterList.add(objectives);
        parameterList.add(yearObjectives + "-" + monthObjectives + "-" + dayobjectioves);
        parameterList.add(notes);

        loginPage.setUsername(mentor).setPassword("pass").clickSubmit();
        headerPage.clickRoadMap().
                clickRoadmapDiscussion();

        roadMapPage.setUser(mentee)
                .createNewRoadMap()
                .sendDate(dayDiscussion, monthDiscussion, yearDiscussion)
                .sendAutolevel(automationLevel)
                .sendManualLevel(manualLevel)
                .sendObjectivesDate(dayobjectioves, monthObjectives, yearObjectives)
                .writeNote(notes)
                .writeObjectives(objectives)
                .submitRoadMap()
                .confirmsubmitRoadMap();

        List<String> returnedRowContent = roadMapPage.getTextFromRoadmapElements();
        System.out.println(parameterList);
        System.out.println(returnedRowContent);
        assertThat("Lists do not contain the same elements.",
                parameterList, containsInAnyOrder(returnedRowContent.toArray(new String[returnedRowContent.size()])));
    }

    @Test(priority = 1)
    public void editRoadMap() {

        String mentor = "arus";
        String mentee = "vrozsa";

        String dayDiscussion = "01";
        String monthDiscussion = "07";
        String yearDiscussion = "2019";

        String dayobjectioves = "09";
        String monthObjectives = "07";
        String yearObjectives = "2019";

        String automationLevel = ExperienceLevel.JUNIOR_ENTRY.getLevel();
        String manualLevel = ExperienceLevel.JUNIOR_ENTRY.getLevel();

        String notes = "niciuna";
        String objectives = "orice alt ceva";

        List<String> parameterList = new ArrayList<>();
        parameterList.add(yearDiscussion + "-" + monthDiscussion + "-" + dayDiscussion);
        parameterList.add(mentor);
        parameterList.add(automationLevel);
        parameterList.add(manualLevel);
        parameterList.add(objectives);
        parameterList.add(yearObjectives + "-" + monthObjectives + "-" + dayobjectioves);
        parameterList.add(notes);

        loginPage.setUsername(mentor).setPassword("pass").clickSubmit();
        headerPage.clickRoadMap().
                clickRoadmapDiscussion();

        roadMapPage.setUser(mentee)
                .clickToEditRoadMap()
                .clearDate()
                .sendDate(dayDiscussion, monthDiscussion, yearDiscussion)
                .sendAutolevel(automationLevel)
                .sendManualLevel(manualLevel)
                .sendObjectivesDate(dayobjectioves, monthObjectives, yearObjectives)
                .writeNote(notes)
                .writeObjectives(objectives)
                .submitRoadMap()
                .confirmsubmitRoadMap();

        List<String> roadmapData = roadMapPage.getTextFromRoadmapElements();

        assertThat("Lists do not contain the same elements.",
                parameterList, containsInAnyOrder(roadmapData.toArray(new String[roadmapData.size()])));

    }

    @AfterMethod
    public void logout() {
        headerPage.clickLogout();
    }


}
