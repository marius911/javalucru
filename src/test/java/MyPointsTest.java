import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import utils.*;

import java.io.IOException;
import java.util.ArrayList;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.*;


public class MyPointsTest extends BaseAbstract {


    @Test
    public void checkUserReceivedtheGivenPoints() {
        String userCareDaPuncte = Users.VROZSA.getNume();
        String userCarePrimeste = Users.ARUS.getNume();
        String criteria = PointsCriteriaQA.FEEDBACK.getCriteria();
        String nrPuncte = "3";
        String mesajulMeeu = "bla bla";

        ArrayList<String> param = new ArrayList<>();
        param.add(userCareDaPuncte);
        param.add(nrPuncte);
        param.add(mesajulMeeu);
        param.add(TodayDate.today());


        loginPage.setUsername(Usernames.VROZSA.getNume())
                .setPassword("pass")
                .clickSubmit();
        offerPointsPage.typeUser(userCarePrimeste)
                .setCriteria(criteria)
                .setpoints(nrPuncte);
        offerPointsPage.writeComment(mesajulMeeu)
                .clickSubmit();
        offerPointsPage.getConfirmationMessage();

        headerPage.clickLogout();

        loginPage.logInaUser(Usernames.ARUS.getNume());
        headerPage.gotoMyPoints();
        System.out.println(param);
        System.out.println(myPointsPage.getLastReceivedPoints());
        myPointsPage.getLastReceivedPoints();

        assertThat("Points or details given are not the same as the points or details received by the recipient", param, containsInAnyOrder(myPointsPage.getLastReceivedPoints().toArray()));
    }

    @Test
    public void checkInMyPointsIfUsersCanGivePointstoHimself() {

        String user = Users.ADMIN.getNume();
        String criteria = PointsCriteriaQA.FEEDBACK.getCriteria();
        String nrPuncte = "3";
        String mesajulMeeu = "bla bla";


        loginPage.setUsername(Usernames.ADMIN.getNume())
                .setPassword("pass")
                .clickSubmit();
        offerPointsPage.typeUser(user)
                .setCriteria(criteria)
                .setpoints(nrPuncte);
        offerPointsPage.writeComment(mesajulMeeu)
                .clickSubmit();
        offerPointsPage.closeWarning();

        headerPage.clickLogout();

        loginPage.logInaUser(Usernames.ADMIN.getNume());
        headerPage.gotoMyPoints();
        myPointsPage.arePointsInMyPoints();

        assertThat("Points have been saved even if page said otherwise", myPointsPage.arePointsInMyPoints(), is(false));
    }

    @AfterMethod
    public void logOut() {
        headerPage.clickLogout();
    }
}
