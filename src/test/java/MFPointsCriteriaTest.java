import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;
import utils.UsersDataProvider;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.*;

public class MFPointsCriteriaTest extends BaseAbstract {



    @Test(dataProvider = "allUsers", dataProviderClass = UsersDataProvider.class)
    public void TestCriteriaDropDownandDetails(String username, String role) {
        loginPage.setUsername(username).setPassword("pass").clickSubmit();
        offerPointsPage.typeUser(username)
                .expandCriteria();
        assertThat(offerPointsPage.getCriteriaDropDown(), equalTo(offerPointsPage.getCriteriaDetails()));

    }

    @AfterMethod
    public void logout() {
        headerPage.clickLogout();
    }


}
