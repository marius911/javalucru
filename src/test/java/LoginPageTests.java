import org.testng.annotations.Test;
import utils.UsersDataProvider;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.*;

public class LoginPageTests extends BaseAbstract {


    @Test(dataProvider = "allUsers", dataProviderClass = UsersDataProvider.class)
    public void testAllUserCanLogin(String user, String role) {
        loginPage.clearUsername()
                .setUsername(user)
                .clearPassword()
                .setPassword("pass")
                .clickSubmit();
        String userRoleInfo = headerPage.getUserAndRoleInfo();
        headerPage.clickLogout();
        assertThat("user can't log", userRoleInfo, allOf(containsString(user), containsString(role)));

    }

    @Test(dataProvider = "allUsers", dataProviderClass = UsersDataProvider.class)
    public void testUserCantLoginWithEmptyPassword(String user, String role) {
        loginPage.clearUsername()
                .setUsername(user)
                .clearPassword()
                .setPassword("")
                .clickSubmit();
        assertThat("user can log in with empty password", loginPage.geterrormessage(), equalToIgnoringCase("Username or password incorrect!"));
    }


}