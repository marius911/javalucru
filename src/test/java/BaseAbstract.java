import org.openqa.selenium.WebDriver;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeSuite;
import pages.*;
import utils.DriverFactory;
import utils.DriverType;
import utils.RestartDB;

import java.io.IOException;

public abstract class BaseAbstract {

    protected WebDriver driver;

    LoginPage loginPage;
    HeaderPage headerPage;
    OfferPointsPage offerPointsPage;
    MyPointsPage myPointsPage;
    RoadMapPage roadMapPage;
    RoadMapCalendarPages roadMapCalendarPages;

    @BeforeSuite
    public void cleanDB() {
        try {
            RestartDB.stopDB();
            RestartDB.startDB();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    @BeforeClass
    public void setUp() {

        driver = DriverFactory.getDriver(DriverType.CHROME);
        driver.get("http://localhost:8091/#!mfview");
        driver.manage().window().maximize();
        loginPage = new LoginPage(driver);
        headerPage = new HeaderPage(driver);
        offerPointsPage = new OfferPointsPage(driver);
        myPointsPage = new MyPointsPage(driver);
        roadMapPage = new RoadMapPage(driver);
        roadMapCalendarPages = new RoadMapCalendarPages(driver);

    }

    @AfterClass
    public void close() {
        driver.quit();
    }


}
