import org.testng.annotations.AfterMethod;
import org.testng.annotations.Test;
import pages.CommonPages;
import utils.UsersDataProvider;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.*;
import static org.hamcrest.Matchers.containsString;

public class UserRightsTest extends BaseAbstract {


    @Test(dataProvider = "communitymanagers", dataProviderClass = UsersDataProvider.class)
    public void testComManagersHaveAdministration(String username, String role) {
        loginPage.clearUsername()
                .setUsername(username)
                .setPassword("pass")
                .clickSubmit();
        boolean hasAdministration = headerPage.hasAdministration();
//        headerPage.clickLogout();
        assertThat("User doesn't administration", hasAdministration, is(true));
    }

    @Test(dataProvider = "users", dataProviderClass = UsersDataProvider.class)
    public void testUsersDontHaveAdministration(String username, String role) {
        loginPage.clearUsername()
                .setUsername(username)
                .setPassword("pass")
                .clickSubmit();
        boolean hasAdministration = headerPage.hasAdministration();
//        headerPage.clickLogout();
        assertThat("User doesn't administration", hasAdministration, is(false));
    }


    @Test(dataProvider = "allUsers", dataProviderClass = UsersDataProvider.class)
    public void testMFOptions(String username, String role) {
        loginPage.setUsername(username)
                .setPassword("pass")
                .clickSubmit();
        String administrationOptionsContent = headerPage.getMFOptions();
        assertThat("MF Points Menu doesn't contain all options", administrationOptionsContent, allOf(containsString("Offer Points"), containsString("My points")));
    }


    @Test(dataProvider = "comPeopleAdminHR", dataProviderClass = UsersDataProvider.class)
    public void testAdminCommunPeopleHrHaveMfStat(String username, String role) {
        loginPage.setUsername(username)
                .setPassword("pass")
                .clickSubmit();
        String administrationOptionsContent = headerPage.getMFOptions();
        assertThat("This user should have access at administration and it doesn't", administrationOptionsContent, containsString("Statistics"));
    }

    @Test(dataProvider = "users", dataProviderClass = UsersDataProvider.class)
    public void testUsersDontHaveMfStat(String username, String role) {
        loginPage.setUsername(username)
                .setPassword("pass")
                .clickSubmit();
        String administrationOptionsContent = headerPage.getMFOptions();
        assertThat("User has Statistics button and it shouldn't", administrationOptionsContent, not(containsString("Statistics")));
    }


    @Test(dataProvider = "allUsers", dataProviderClass = UsersDataProvider.class)
    public void testRoadMapOptions(String username, String role) {
        loginPage.setUsername(username)
                .setPassword("pass")
                .clickSubmit();
        String roadmapContent = headerPage.getRoadMapOptions();
        assertThat("RoadMap menu doesn't contain all options", roadmapContent, allOf(containsString("My Roadmap"), containsString("Roadmap Discussions"), containsString("Roadmap Calendar")));
    }

    @AfterMethod
    public void logout() {
        headerPage.clickLogout();
    }

}
