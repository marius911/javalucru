import org.testng.annotations.Test;
import utils.ExperienceLevel;

import java.util.ArrayList;
import java.util.List;

public class RoadMapCalendarTests extends BaseAbstract {

    @Test
    public void testRoadMapCalendarEntry() {
        String mentor = "arus";
        String mentee = "vrozsa";

        String dayDiscussion = "29";
        String monthDiscussion = "07";
        String yearDiscussion = "2019";

        String dayobjectioves = "30";
        String monthObjectives = "07";
        String yearObjectives = "2019";

        String automationLevel = ExperienceLevel.JUNIOR_ENTRY.getLevel();
        String manualLevel = ExperienceLevel.JUNIOR_ENTRY.getLevel();

        String notes = "niciuna";
        String objectives = "orice alt ceva";

        List<String> parameterList = new ArrayList<>();


        parameterList.add(yearObjectives + "-" + monthObjectives + "-" + dayobjectioves);
        parameterList.add(mentee);

        loginPage.setUsername(mentor).setPassword("pass").clickSubmit();
        headerPage.clickRoadMap().
                clickRoadmapDiscussion();

        roadMapPage.setUser(mentee)
                .createNewRoadMap()
                .clearDate()
                .sendDate(dayDiscussion, monthDiscussion, yearDiscussion)
                .sendAutolevel(automationLevel)
                .sendManualLevel(manualLevel)
                .sendObjectivesDate(dayobjectioves, monthObjectives, yearObjectives)
                .writeNote(notes)
                .writeObjectives(objectives)
                .submitRoadMap()
                .confirmsubmitRoadMap();

        headerPage.clickLogout();

        loginPage.logInaUser(mentee);
        headerPage.goToRoadMapCalendar();

        System.out.println(parameterList);
        System.out.println(roadMapCalendarPages.getLastCalendarEntry());
    }
}
