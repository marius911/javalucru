package pages;

import com.thedeanda.lorem.Lorem;
import com.thedeanda.lorem.LoremIpsum;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.*;
import java.util.concurrent.TimeUnit;


public class OfferPointsPage extends CommonPages {

    @FindBy(xpath = "//*[@id=\"employeeSelect\"]/div")
    WebElement exapandUserDropDown;

    @FindBy(xpath = "//*[@id=\"criteriaSelect\"]/div")
    WebElement exapandCriteria;

    @FindBy(xpath = "//*[@id=\"pointsSelect\"]/div")
    WebElement exapandCriteriaPoints;


    @FindBy(css = "div[id*='COMBOBOX']")
    WebElement popUpMenu;

    @FindBy(xpath = "//*[@id='employeeSelect']/input")
    WebElement selectColleague;

    @FindBy(xpath = "//*[@id='criteriaSelect']/input")
    WebElement selectCriteria;

    @FindBy(xpath = "//*[@id='pointsSelect']/input")
    WebElement selectPoints;

    @FindBy(xpath = "//textarea[@id=\"details\"]")
    WebElement commentInput;

    @FindBy(id = "submitPoints")
    WebElement submitButton;

/*    @FindBy(css = ".v-Notification-caption")
    WebElement cannotOfferPointsyourselfWarning;*/

    @FindBy(xpath = "//*[@id=\"ROOT-2521314-overlays\"]/div[2]/div")
    WebElement savedPointsConfirmation;

    WebDriverWait wait = new WebDriverWait(driver, 5);
    Actions actions = new Actions(driver);

    public OfferPointsPage(WebDriver driver) {
        super(driver);
    }

    public ArrayList<String> getCriteriaDropDown() {

        String users = popUpMenu.getText();
        String criteriaInDropDown[] = users.split("\\n");
        for (int i = 0; i < criteriaInDropDown.length; i++) {

            criteriaInDropDown[i] = criteriaInDropDown[i].trim();

        }
        ArrayList<String> critList = new ArrayList<>();
        Collections.addAll(critList, criteriaInDropDown);

        System.out.println(critList);
        return critList;
    }

    public ArrayList<String> getCriteriaDetails() {
        List<WebElement> crit = driver.findElements(By.cssSelector("div[id=legend] b"));

        ArrayList<String> criterii = new ArrayList<>();
        for (WebElement i : crit
        ) {
            criterii.add(i.getText().substring(0, i.getText().length() - 1));
        }

        //System.out.println(criterii);
        return criterii;
    }

    public OfferPointsPage typeUser(String username) {
        wait.until(ExpectedConditions.visibilityOf(selectColleague));
        selectColleague.sendKeys(username);
        wait.until(ExpectedConditions.visibilityOf(popUpMenu));
        selectColleague.sendKeys(Keys.ENTER);
        selectColleague.sendKeys(Keys.ENTER);

        return this;

    }


    public OfferPointsPage expandPoints() {
        if (selectPoints.isEnabled()) {
            wait.until(ExpectedConditions.visibilityOf(exapandCriteriaPoints));
            exapandCriteriaPoints.click();
        }
        // System.out.println(popUpMenu.getText());
        return this;
    }

    public ArrayList<String> getPointsDropDown() {
        if (selectPoints.isEnabled()) {
            String users = popUpMenu.getText();
            String criteriaInDropDown[] = users.split("\\n");
            for (int i = 0; i < criteriaInDropDown.length; i++) {

                criteriaInDropDown[i] = criteriaInDropDown[i].trim();

            }


            ArrayList<String> critList = new ArrayList<>();
            Collections.addAll(critList, criteriaInDropDown);


            return critList;
        }
        return null;
    }


    public OfferPointsPage expandCriteria() {
        wait.until(ExpectedConditions.visibilityOf(exapandCriteria));
        exapandCriteria.click();
        // System.out.println(popUpMenu.getText());
        return this;
    }

    public OfferPointsPage setCriteria(String criteria) {
        wait.until(ExpectedConditions.visibilityOf(selectCriteria));
        selectCriteria.click();
        selectCriteria.sendKeys(criteria);
        driver.manage().timeouts().implicitlyWait(3, TimeUnit.SECONDS);
        selectCriteria.sendKeys(Keys.ENTER);
        selectCriteria.sendKeys(Keys.ENTER);


        return this;
    }

    public void setRandomCriteria() {
        Random rand = new Random();
        ArrayList<String> list = getCriteriaDropDown();
        String criteron = list.get(rand.nextInt(list.size()));
        setCriteria(criteron);
    }

    public OfferPointsPage setpoints(String points) {
        if (selectPoints.isEnabled()) {
            selectPoints.click();
            selectPoints.sendKeys(points);
            selectPoints.sendKeys(Keys.ENTER);
            selectPoints.sendKeys(Keys.ENTER);
        }

        return this;
    }

    public void setRandomPoints() {
        if (selectPoints.isEnabled()) {
            Random rand = new Random();
            ArrayList<String> list = getPointsDropDown();
            String points = list.get(rand.nextInt(list.size()));
            setpoints(points);
        }
    }


    public OfferPointsPage writeComment(String comment) {
        commentInput.sendKeys(comment);
        return this;
    }

    public OfferPointsPage writeRandomComment() {
        Lorem lorem = LoremIpsum.getInstance();
        writeComment(lorem.getWords(2, 8));
        return this;
    }

    public void clickSubmit() {
        wait.until(ExpectedConditions.elementToBeClickable(submitButton));
        actions.moveToElement(submitButton).click().perform();
        submitButton.click();
    }

    public String getWarning() {
        wait.until(ExpectedConditions.visibilityOf(savedPointsConfirmation));
        // System.out.println(cannotOfferPointsyourselfWarning.getText());
        return savedPointsConfirmation.getText();
    }

    public String getConfirmationMessage() {
        wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//*[@id=\"ROOT-2521314-overlays\"]/div[2]/div")));
        String waring = savedPointsConfirmation.getText();

        return waring;

    }

    public void closeWarning(){

        WebElement warning = driver.findElement(By.xpath("//*[@id=\"ROOT-2521314-overlays\"]/div[2]"));
        wait.until(ExpectedConditions.visibilityOf(warning));
        actions.moveToElement(warning).click().perform();
        driver.navigate().refresh();



    }

}


