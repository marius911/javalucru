package pages;

import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.TimeUnit;


public class RoadMapPage extends CommonPages {

    @FindBy(xpath = "//*[@id=\"ROOT-2521314\"]/div/div[2]/div/div[3]/div/div[2]/div/div[3]/div/div/div/div[2]/input")
    WebElement setUserRoadMap;

    @FindBy(id = "newRoadmap")
    WebElement newRoadMap;

    @FindBy(xpath = "//*/div[1]/div/div/div[1]/div/div[1]/div/div[2]/input")
    WebElement discussionDate;

    @FindBy(xpath = "//*/div[1]/div/div/div[1]/div/div[5]/div/div[2]/input")
    WebElement automationLevel;


    @FindBy(xpath = "//*/div[1]/div/div/div[1]/div/div[9]/div/div[2]/input")
    WebElement objectivesDateField;

    @FindBy(xpath = "//*/div[1]/div/div/div[1]/div/div[7]/div/div[2]/input")
    WebElement manualLevel;

    @FindBy(xpath = "//*[@id=\"notesArea\"]")
    WebElement notesArea;

    @FindBy(css = "#objectives > iframe")
    WebElement setObjectives;

    @FindBy(xpath = "//*[@id=\"submitRoadmap\"]")
    WebElement submitButton;

    @FindBy(xpath = "//*[@id=\"confirmationModal\"]/div/div/div[3]/div/div/div[3]/div/div[1]/div")
    WebElement confirmButton;

    @FindBy(xpath = "//*[@id=\"lastRoadmapGrid\"]/div[3]/table/tbody/tr")
    WebElement lastRoadMapRow;

    @FindBy(xpath = "//*[@id=\"lastRoadmapGrid\"]/div[3]/table/tbody/tr/*")
    List<WebElement> roadMapDataList;

    @FindBy(xpath = "//*[@id=\"lastRoadmapGrid\"]/div[3]/table/tbody/tr/td[1]")
    WebElement editRoadmapCell;

    WebDriverWait wait = new WebDriverWait(driver, 5);
    Actions actions = new Actions(driver);

    public RoadMapPage(WebDriver driver) {
        super(driver);
    }

    public RoadMapPage setUser(String username) {
        wait.until(ExpectedConditions.visibilityOf(setUserRoadMap));
        setUserRoadMap.click();
        setUserRoadMap.sendKeys(username);
        setUserRoadMap.sendKeys(Keys.ENTER);
        setUserRoadMap.sendKeys(Keys.ENTER);
        return this;
    }

    public RoadMapPage createNewRoadMap() {

        wait.until(ExpectedConditions.elementToBeClickable(newRoadMap));
        actions.moveToElement(newRoadMap).click().perform();
        return this;
    }

    public RoadMapPage sendDate(String day, String month, String year) {
        wait.until(ExpectedConditions.visibilityOf(discussionDate));
        discussionDate.sendKeys(day + "/" + month + "/" + year);
        return this;
    }

    public RoadMapPage clearDate() {
        wait.until(ExpectedConditions.visibilityOf(discussionDate));
        discussionDate.clear();
        return this;
    }

    public RoadMapPage sendAutolevel(String automationLev) {
        wait.until(ExpectedConditions.visibilityOf(automationLevel));
        automationLevel.click();
        automationLevel.sendKeys(automationLev);
        automationLevel.sendKeys(Keys.ENTER);
        automationLevel.sendKeys(Keys.ENTER);

        return this;

    }

    public RoadMapPage sendManualLevel(String manualLev) {
        wait.until(ExpectedConditions.visibilityOf(manualLevel));
        manualLevel.click();
        manualLevel.sendKeys(manualLev);
        // driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        manualLevel.sendKeys(Keys.ENTER);
        manualLevel.sendKeys(Keys.ENTER);

        return this;

    }

    public RoadMapPage sendObjectivesDate(String day, String month, String year) {
        wait.until(ExpectedConditions.visibilityOf(objectivesDateField));
        objectivesDateField.clear();
        objectivesDateField.sendKeys(day + "/" + month + "/" + year);

        return this;
    }


    public RoadMapPage writeNote(String note) {
        wait.until(ExpectedConditions.visibilityOf(notesArea));
        notesArea.clear();
        notesArea.sendKeys(note);
        return this;
    }


    public RoadMapPage writeObjectives(String objectives) {
        wait.until(ExpectedConditions.elementToBeClickable(setObjectives));
        setObjectives.sendKeys(Keys.chord(Keys.CONTROL, "a", Keys.DELETE));

        setObjectives.sendKeys(objectives);


        return this;
    }


    public RoadMapPage submitRoadMap() {
        wait.until(ExpectedConditions.elementToBeClickable(submitButton));
        submitButton.click();
        return this;
    }

    public void confirmsubmitRoadMap() {
        wait.until(ExpectedConditions.elementToBeClickable(confirmButton));
        actions.moveToElement(confirmButton).click().perform();

    }

/*
    public List<String> getTextOfLastRoadMap() {
        wait.until(ExpectedConditions.visibilityOf(lastRoadMapRow));
        String roadmapInfo = lastRoadMapRow.getText();
        String rowContent[] = roadmapInfo.split("\\n");
        for (int i = 0; i < rowContent.length; i++) {

            rowContent[i] = rowContent[i].trim();
        }

        List<String> rowContentList = new ArrayList<>();
        Collections.addAll(rowContentList, rowContent);

        return rowContentList;

    }
*/

    public List<String> getTextFromRoadmapElements() {
        wait.until(ExpectedConditions.visibilityOf(lastRoadMapRow));
        List<String> roadmapData = new ArrayList<>();

        for (WebElement e : roadMapDataList) {
            roadmapData.add(e.getText());
        }

        return roadmapData;
    }

    public RoadMapPage clickToEditRoadMap() {
        wait.until(ExpectedConditions.visibilityOf(editRoadmapCell));
        actions.moveToElement(editRoadmapCell).doubleClick().perform();
        return this;
    }

}
