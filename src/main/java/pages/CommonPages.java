package pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;

public abstract class CommonPages {

    protected WebDriver driver;


    public CommonPages(WebDriver driver) {
        PageFactory.initElements(driver, this);
        this.driver = driver;
    }
}
