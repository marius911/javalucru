package pages;

import org.openqa.selenium.TimeoutException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;

public class HeaderPage extends CommonPages {
    public HeaderPage(WebDriver driver) {
        super(driver);
    }

    @FindBy(xpath = "//*[contains(text(),'Logout')] ")
    WebElement logoutButton;

    @FindBy(xpath = "//span[contains(@class, 'v-menubar-menuitem-caption') and text() = 'Administration']")
    WebElement administrationButton;

    @FindBy(xpath = "//span[contains(@class, 'v-menubar-menuitem-caption') and text() = 'Multam Fain Points']")
    WebElement mfPointsButton;

    @FindBy(xpath = "//span[contains(@class, 'v-menubar-menuitem-caption') and text() = 'Roadmap']")
    WebElement roadMapButton;

    @FindBy(css = "div.v-menubar-popup")
    WebElement popUpMForRoadmap;


    @FindBy(css = "span.v-menubar-menuitem-disabled")
    WebElement userRole;

    @FindBy(css = "#ROOT-2521314-overlays > div.v-menubar-popup > div > div > span:nth-child(2) > span")
    WebElement roadMapDiscussion;

    @FindBy(xpath = "//*[@id=\"ROOT-2521314-overlays\"]/div[2]/div/div/span[1]/span")
    WebElement myPointsButon;

    @FindBy(xpath = "//*[@id=\"ROOT-2521314-overlays\"]/div[2]/div/div/span[3]/span")
    WebElement roadMapCalendar;

    WebDriverWait wait = new WebDriverWait(driver, 5);
    Actions actions = new Actions(driver);

    public HeaderPage clickLogout() {
        driver.navigate().refresh();
        wait.until(ExpectedConditions.elementToBeClickable(logoutButton));
        actions.moveToElement(logoutButton).click().perform();
        return this;
    }

    public String getUserAndRoleInfo() {

        try {
            wait.until(ExpectedConditions.visibilityOf(userRole));
        } catch (TimeoutException e) {
            Assert.fail("userul nu a ajuns pe pagina logata");
        }
        return userRole.getText();
    }

    public String getRoadMapOptions() {
        wait.until(ExpectedConditions.visibilityOf(roadMapButton));
        actions.moveToElement(roadMapButton).click().perform();

        wait.until(ExpectedConditions.visibilityOf(popUpMForRoadmap));
        return popUpMForRoadmap.getText();
    }

    public HeaderPage clickOnMF() {
        wait.until(ExpectedConditions.visibilityOf(mfPointsButton));
        actions.moveToElement(mfPointsButton).click().perform();
        wait.until(ExpectedConditions.visibilityOf(popUpMForRoadmap));
        return this;
    }

    public String getMFOptions() {
        clickOnMF();

        return popUpMForRoadmap.getText();
    }

    public boolean hasAdministration() {

        try {
            wait.until(ExpectedConditions.visibilityOf(administrationButton));
        } catch (TimeoutException e) {
            return false;
        }
        return administrationButton.isDisplayed();
    }

    public HeaderPage clickRoadMap() {
        wait.until(ExpectedConditions.visibilityOf(roadMapButton));
        actions.moveToElement(roadMapButton).click().perform();
        return this;
    }

    public HeaderPage clickRoadmapDiscussion() {
        wait.until(ExpectedConditions.visibilityOf(roadMapDiscussion));
        actions.moveToElement(roadMapDiscussion).click().perform();
        wait.until(ExpectedConditions.visibilityOf(popUpMForRoadmap));
        return this;
    }

    public HeaderPage gotoMyPoints() {
        clickOnMF();
        actions.moveToElement(myPointsButon).click().perform();
        return this;
    }

    public HeaderPage goToRoadMapCalendar() {
        clickRoadMap();

        wait.until(ExpectedConditions.elementToBeClickable(roadMapCalendar));
        actions.moveToElement(roadMapCalendar).click().perform();


        return this;
    }


}
