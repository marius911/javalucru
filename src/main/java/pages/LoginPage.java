package pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.concurrent.TimeUnit;

public class LoginPage extends CommonPages {

    public LoginPage(WebDriver driver) {
        super(driver);
    }

    @FindBy(css = "input[id=username]")
    WebElement usernameField;

    @FindBy(id = "password")
    WebElement passwordField;

    @FindBy(id = "submitLogin")
    WebElement submitButton;

    @FindBy(xpath = "//*[@id=\"ROOT-2521314-overlays\"]/div[2]/div[1]/div[1]/h1[1]")
    WebElement incorrectUserOrPassword;

    WebDriverWait wait = new WebDriverWait(driver, 5);
    Actions actions = new Actions(driver);


    public LoginPage setUsername(String username) {
        wait.until(ExpectedConditions.visibilityOf(usernameField));
        usernameField.sendKeys(username);
        return this;
    }

    public LoginPage clearUsername() {
        wait.until(ExpectedConditions.visibilityOf(usernameField));
        usernameField.clear();
        return this;
    }

    public LoginPage setPassword(String password) {

        passwordField.sendKeys(password);
        return this;
    }

    public LoginPage clearPassword() {

        passwordField.clear();
        return this;
    }

    public void clickSubmit() {

        submitButton.click();

    }

    public String geterrormessage() {

        wait.until(ExpectedConditions.visibilityOf(incorrectUserOrPassword));
        return incorrectUserOrPassword.getText();
    }

    public void logInaUser(String user) {
        setUsername(user);
        setPassword("pass");
        clickSubmit();
    }

}
