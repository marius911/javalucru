package pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.ArrayList;
import java.util.Collections;

public class MyPointsPage extends CommonPages {
    @FindBy(xpath = "//*[@id=\"pointsAccordion\"]/div[1]/div[2]/div[1]/div[1]")
    WebElement lastReceivedMF;
    WebDriverWait wait = new WebDriverWait(driver, 5);
    Actions actions = new Actions(driver);

    public MyPointsPage(WebDriver driver) {
        super(driver);
    }

    public ArrayList<String> getLastReceivedPoints() {
        wait.until(ExpectedConditions.visibilityOf(lastReceivedMF));
        //System.out.println(lastReceivedMF.getText());
        String lastReceived = lastReceivedMF.getText();

        String lastReceivedArray[] = lastReceived.split("\\n");

        for (int i = 3; i < lastReceivedArray.length; i++) {
            if (lastReceivedArray[i] == "Received from" || lastReceivedArray[i] == "At date" || lastReceivedArray[i] == "With details") {
                 lastReceivedArray[i] = "";
            } else {
                lastReceivedArray[i] = lastReceivedArray[i].trim();
            }

        }

        ArrayList<String> list = new ArrayList<>();
        Collections.addAll(list, lastReceivedArray);


        return list;
    }

    public boolean arePointsInMyPoints(){
        wait.until(ExpectedConditions.visibilityOf(lastReceivedMF));
        return lastReceivedMF.isDisplayed();
    }
}
