package pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import java.util.List;

public class FooterPage extends CommonPages {

/*
    @FindBy(xpath = "//a")
    WebElement links;
*/

    public FooterPage(WebDriver driver) {
        super(driver);
    }

    public FooterPage checkLinks() {
        List<WebElement> link = driver.findElements(By.xpath("\\a"));

        return this;

    }


}
