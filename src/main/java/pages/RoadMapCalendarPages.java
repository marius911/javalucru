package pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class RoadMapCalendarPages extends CommonPages {

    @FindBy(xpath = "//*[@id=\"upcoming\"]/div[3]/table/tbody/tr")
    WebElement roadmapCalendarEntry;

    public RoadMapCalendarPages(WebDriver driver) {
        super(driver);
    }

    WebDriverWait wait = new WebDriverWait(driver, 5);
    Actions actions = new Actions(driver);

    public List<String> getLastCalendarEntry() {
        wait.until(ExpectedConditions.visibilityOf(roadmapCalendarEntry));
        String rowContent = roadmapCalendarEntry.getText();

        String rowContentArr[] = rowContent.split("\\n");
        for (int i = 0; i < rowContentArr.length; i++) {

            rowContentArr[i] = rowContentArr[i].trim();
        }

        List<String> rowContentList = new ArrayList<>();
        Collections.addAll(rowContentList, rowContent);

        return rowContentList;

    }
}
