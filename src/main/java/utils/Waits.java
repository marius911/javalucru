package utils;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.WebDriverWait;
import pages.CommonPages;

public class Waits extends CommonPages {


    WebDriverWait wait = new WebDriverWait(driver, 5);
    Actions actions = new Actions(driver);

    public Waits(WebDriver driver) {
        super(driver);
    }
}
