package utils;

public enum Usernames {


    ADMIN ("admin"),
    ARUS ("arus"),
    SCAPALNEAN ("scaplnean"),
    VROZSA ("vrozsa"),
    EPARLEA ("eparlea"),
    ;

    String nume;
    Usernames( String nume) {
        this.nume = nume;
    }

    public String getNume() {
        return nume;
    }
}


