package utils;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;


public class RestartDB{

    public static void stopDB()throws IOException,InterruptedException{
        String cmd="/home/training/Apps/qamanager/qamanager stop";

        Process proc=Runtime.getRuntime().exec(cmd);

        BufferedReader reader=new BufferedReader(new InputStreamReader(proc.getInputStream()));

        String line="";
        while((line=reader.readLine())!=null){
            System.out.print(line+"\n");

            proc.waitFor();

        }
    }

    public static void startDB()throws IOException,InterruptedException{
        String cmd="/home/training/Apps/qamanager/qamanager start";

        Process proc=Runtime.getRuntime().exec(cmd);

        BufferedReader reader=new BufferedReader(new InputStreamReader(proc.getInputStream()));

        String line="";
        while((line=reader.readLine())!=null){
            System.out.print(line+"\n");

            proc.waitFor();


        }
    }
}

