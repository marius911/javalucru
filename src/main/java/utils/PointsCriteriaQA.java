package utils;

public enum PointsCriteriaQA {
    FEEDBACK("Feedback process participation"),
    INTERNSHIP("Internship contribution"),
    INTERVIEW("Interview participation"),
    MOODLE_DOC("Moodle documentation contribution"),
    OTHER("Other"),
    PRESENTATION("Presentation"),
    PROCESS_CONTRIB("Processes contribution"),
    QUIZZ_REVIEW("Quiz review"),
    ROADMAP_MENTORING("Roadmap mentoring"),
    SUPPORT("Support"),
    TOOLS_CONTRIB ("Tools Contribution");

    String criteria;
    PointsCriteriaQA(String criteria) {
        this.criteria = criteria;
    }

    public String getCriteria() {
        return criteria;
    }
}
