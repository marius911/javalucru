package utils;

import org.testng.annotations.DataProvider;

import java.util.ArrayList;

public class UsersDataProvider {

    @DataProvider(name = "allUsers")
    public Object[][] UsersAll() {
        return new Object[][]{

                {"arus", "user"},
                {"dgliga", "user"},
                {"ioana.dardai", "PEOPLE MANAGER"},
                {"bogdan.tiu", "user"},
                {"mihai.botez", "user"},
                {"scapalnean", "people manager"},
                {"vrozsa", "community manager"},
                {"eparlea", "hr"},
                {"ramona.raicu", "community manager"},
                {"admin", "admin"},

        };
    }

    @DataProvider(name = "allUsersNames")
    public Object[][] UsersNamesAll() {
        return new Object[][]{
                {"Paul Precup"},
                {"Alexandra Rus"},
                {"Dragos Gliga"},
                {"Ioana Dardai"},
                {"Bogdan Tiu"},
                {"Mihai Botez"},
                {"Sorana Capalnean"},
                {"Vlad Rozsa"},
                {"Simona Parlea"},
                {"Ramona Raicu"},

        };
    }
    //, "Alexandra Rus", "Bogdan Tiu", "Dragos Gliga", "Ioana Dardai", "Mihai Botez", "Ramona Raicu", "Vlad Rozsa", "Simona Parlea", "Ramona Raicu"))));


    @DataProvider(name = "users")
    public Object[][] Users() {
        return new Object[][]{
                {"arus", "user"},
                {"dgliga", "user"},
                {"bogdan.tiu", "user"},
                {"mihai.botez", "user"},
        };
    }

    @DataProvider(name = "peoplemanagers")
    public Object[][] PeopleManagers() {
        return new Object[][]{
                {"ioana.dardai", "PEOPLE MANAGER"},
                {"scapalnean", "people manager"},

        };
    }

    @DataProvider(name = "communitymanagers")
    public Object[][] ComminityManagers() {
        return new Object[][]{
                {"vrozsa", "community manager"},
                {"ramona.raicu", "community manager"},
                {"admin", "admin"}

        };
    }

    @DataProvider(name = "hr")
    public Object[][] HR() {
        return new Object[][]{
                {"eparlea", "hr"},
        };
    }

    @DataProvider(name = "comPeopleAdminHR")
    public Object[][] ComPeopleAdminHR() {
        return new Object[][]{
                {"eparlea", "hr"},
                {"ioana.dardai", "PEOPLE MANAGER"},
                {"scapalnean", "people manager"},
                {"vrozsa", "community manager"},
                {"ramona.raicu", "community manager"},
                {"admin", "admin"},


        };
    }

    @DataProvider(name = "allUsernames")
    public Object[][] UsersnamesAll() {
        return new Object[][]{

                {"arus"},
                {"dgliga"},
                {"ioana.dardai"},
                {"bogdan.tiu"},
                {"mihai.botez"},
                {"scapalnean"},
                {"vrozsa"},
                {"eparlea"},
                {"ramona.raicu"},
                {"admin"},

        };
    }


}


