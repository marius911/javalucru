package utils;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;

public class DriverFactory {


    public static final WebDriver getDriver(DriverType type) {


        switch (type) {

            case CHROME:
                System.setProperty("webdriver.chrome.driver", "src/main/resources/chromedriver");
                return new ChromeDriver();
            case FIREFOX:
                System.setProperty("webdriver.gecko.driver", "src/main/resources/geckodriver");
                return new FirefoxDriver();
        }
        return null;
    }
}


