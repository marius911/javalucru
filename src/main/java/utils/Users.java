package utils;

public enum Users {
    ADMIN ("Paul Precup"),
    ARUS ("Alexandra Rus"),
    SCAPALNEAN ("Sorana Caplnean"),
    VROZSA ("Vlad Rozsa"),
    EPARLEA ("Simona Parlea"),
    ;

    String nume;
    Users( String nume) {
        this.nume = nume;
    }

    public String getNume() {
        return nume;
    }
}
