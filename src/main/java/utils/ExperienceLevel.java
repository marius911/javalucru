package utils;

public enum ExperienceLevel {
    JUNIOR_ENTRY("Junior-Entry"),
    JUNIOR_STABLE("Junior-Stable"),
    JUNIOR_TO_MID("Junior-To-Mid"),
    MID_ENTRY("Mid-Entry"),
    MID_STABLE("Mid-Stable"),
    MID_TO_SENIOR("Mid-To-Senior"),
    SENIOR("Senior"),
    TECHNICAL_EXPERT("Technical-Expert");


    private String level;

    ExperienceLevel(String s) {
        level = s;
    }

    public String getLevel() {
        return level;
    }
}
